package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
									.addAnnotatedClass(Student.class).buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			//start transaction
			session.beginTransaction();
			
			//query students
			List<Student> students = session.createQuery("from Student").list();
			
			dispayStudents(students);
			
			students = session.createQuery("from Student s where s.lastName='Doe'").list();

			System.out.println("\n\nStudents with last name eq Doe");
			dispayStudents(students);
			
			
			students = session.createQuery("from Student s where s.email like '%luv2code.com'").list();

			System.out.println("\n\nStudents with last mail luv2code.com");
			dispayStudents(students);
			
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			factory.close();
		}
	}

	private static void dispayStudents(List<Student> students) {
		for (Student s: students){
			System.out.println(s);
		}
	}

}
