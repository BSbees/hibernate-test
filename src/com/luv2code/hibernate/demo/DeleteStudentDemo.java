package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
									.addAnnotatedClass(Student.class).buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			//delete with object
			int studentID = 2;
			session.beginTransaction();
			
			System.out.println("Retrieve student with id = 2");
			Student myStudent = session.get(Student.class, studentID);
			
			System.out.println("Delete the student");
			//session.delete(myStudent);
			
			
			//delete with query
			System.out.println("Delete with id > 1000");
			session.createQuery("delete from Student where id > 1000").executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			factory.close();
		}
	}
}
