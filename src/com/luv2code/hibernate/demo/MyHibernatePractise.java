package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Employee;


public class MyHibernatePractise {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Employee.class).buildSessionFactory();

		Session session = factory.getCurrentSession();
		
		try {
			Employee newEmployee1 = new Employee("Paul", "Test", "Google");
			Employee newEmployee2 = new Employee("John", "Noone", "Microsoft");
			Employee newEmployee3 = new Employee("Jeremy", "Clerk", "Apple");
			
			System.out.println("Creating three new employees");
			session = factory.getCurrentSession();
			session.beginTransaction();
			session.save(newEmployee3);
			session.save(newEmployee2);
			session.save(newEmployee1);
			
			//session.getTransaction().commit();

			//session = factory.getCurrentSession();
			//session.beginTransaction();
			System.out.println("Updating Employee with name Paul");
			session.createQuery("update Employee set company='IBM'").executeUpdate();
			session.getTransaction().commit();
			
			System.out.println("Printing Employes");
			session = factory.getCurrentSession();
			session.beginTransaction();
			List<Employee> employees = session.createQuery("from Employee").list();
			printEmployees(employees);
			session.getTransaction().commit();

			System.out.println("delete all this pancakes");
			session = factory.getCurrentSession();
			session.beginTransaction();
			session.createQuery("delete from Employee where idEmployee > 5").executeUpdate();
			session.getTransaction().commit();
			
			System.out.println("Printing Employes");
			session = factory.getCurrentSession();
			session.beginTransaction();
			employees = session.createQuery("from Employee").list();
			printEmployees(employees);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
		} finally {
			factory.close();
		}
	}

	private static void printEmployees(List<Employee> employees) {
		for (Employee e: employees){
			System.out.println(e);
		}
	}
}
