package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
									.addAnnotatedClass(Student.class).buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			System.out.println("Creating new student object");
			Student newStudent = new Student("Daffy", "Wall", "daffy@luv2code.com");
			
			session.beginTransaction();
			System.out.println("Saving the student");
			System.out.println(newStudent);
			session.save(newStudent);
			
			//commit
			session.getTransaction().commit();
			
			System.out.println("Student saved, id: " + newStudent.getId());
			session = factory.getCurrentSession();
			session.beginTransaction();
			System.out.println("\nGEtting student with id: " + newStudent.getId());
			
			Student readStudent = session.get(Student.class, newStudent.getId());
			System.out.println(readStudent);
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			factory.close();
		}
	}

}
