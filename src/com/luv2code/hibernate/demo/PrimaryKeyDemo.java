package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class).buildSessionFactory();

		Session session = factory.getCurrentSession();
		
		try {
			System.out.println("Creating 3 student objects");
			Student newStudent1 = new Student("John", "Doe", "john@luv2code.com");
			Student newStudent2 = new Student("Mary", "Public", "mary@luv2code.com");
			Student newStudent3 = new Student("Bonita", "Applebum", "bonita@luv2code.com");
			
			session.beginTransaction();
			
			System.out.println("Saving the students");
			session.save(newStudent1);
			session.save(newStudent2);
			session.save(newStudent3);
			
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			factory.close();
		}
	}

}
